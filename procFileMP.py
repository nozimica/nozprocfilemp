import multiprocessing as mp
import os
import math

class ProcFileMP(object):
    """ Class to process a file line by line, by N parallel groups."""

    def __init__(self, nprocs, callback):
        self.nprocs = nprocs
        self.callback = callback

    def procMultFile(self, fileName):
        """ Reads a single file, giving each process a chunk of consecutive lines.

        Args:
            fileName (str): The path of the file to be read.
        """
        # get file size and set chuck size
        filesize = os.path.getsize(fileName)
        # split_size = 1024*1024*1024
        split_size = int(math.ceil(filesize / float(self.nprocs)))

        logging.debug('filesize: %d -- split_size: %d' % (filesize, split_size))

        # create pool, initialize chunk start location (start)
        pool = mp.Pool(processes=self.nprocs)

        start = 0
        argsInput = []
        with open(fileName, 'r') as fh:
            # for every chunk in the file...
            for chunk in xrange(self.nprocs):
                # determine where the chunk ends, is it the last one?
                if start + split_size > filesize:
                    end = filesize
                else:
                    end = start + split_size

                # seek to end of chunk and read next line to ensure you 
                # pass entire lines to the processfile function
                fh.seek(end)
                fh.readline()

                # get current file location
                end = fh.tell()

                logging.debug("Chunk: %d. %d--%d" % (chunk, start, end))

                pool.apply_async(self.callback, args=(fileName, self, start, end))

                # setup next chunk
                start = end

        # close and wait for pool to finish
        pool.close()
        pool.join()

